<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class MyController
{
	public function execute()
	{
	$data = [
		['id' => '0',
		'name' => 'hoge',
		'msg'=>  'Hello!',
		'update_date' => '2018/11/15 12:43:00',
		'create_date' => '2018/11/10 15:19:00'
		],
		['id' => '1',
		'name' => 'huga',
		'msg'=>  'Good night',
		'update_date' => '2018/11/20 16:14:00',
		'create_date' => '2018/11/20 20:54:00'
		]
	];
	return $data;
  }
}

