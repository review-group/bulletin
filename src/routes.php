<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {

// Sample log message
// $this->logger->info("Slim-Skeleton '/' route");

// Render index view
//  return $this->renderer->render($response, 'index.phtml', $args);
});


/**
 *  掲示板にあった書き込みをviewにわたす
 */
$app->get('/Controllers', function (Request $request, Response $response, array $args) {
	$my_controller = new App\Controllers\MyController();
	$data = $my_controller->execute();
	return $this->renderer->render($response, 'MyView.phtml', ['data' => $data]);
});


/**
 *  ・投稿, 編集, 削除 の選択して各viewに遷移
 *  ・1行になった掲示板の内容を1投稿ごとに分けてviewにわたす
 */
$app->post('/Controllers', function (Request $request, Response $response, array $args) {
	$post = $request->getParsedBody();
	$action = $post['action'];
	$data = $post['data'];

	// 初期データの要素数が6個以上なら5個ずつに分ける
	$data_count = count($data);
	$segment_data = [];
	if (5 < $data_count) {
		$segment_data = array_chunk($data, 5);
	}

	 // action によって処理を分ける
	switch ($action) {
		case 'add':
			return $this->renderer->render($response, 'AddView.phtml', ['data' => $segment_data]);
			break;
		case 'edit':
			return $this->renderer->render($response, 'EditView.phtml', ['data' => $segment_data]);
			break;
		case 'del':
			return $this->renderer->render($response, 'DelView.phtml', ['data' => $segment_data]);
			break;
	}
});


/**
 *  ・新規投稿した名前と内容の受け取り
 *  ・1行になった掲示板の内容を1投稿ごとに分けてviewにわたす
*/
$app->post('/Controllers/add', function (Request $request, Response $response, array $args) {
	$post = $request->getParsedBody();
	$name = $post['name'];
	$msg = $post['msg'];
	$data = $post['data'];

	// 初期データの要素数が6個以上なら5個ずつに分ける
	$data_count = count($data);
	$segment_data = [];
	if (5 < $data_count) {
		$segment_data = array_chunk($data, 5);
	}

	// 各配列の最初の要素(id)を取得
	$ids = [];
	foreach ($segment_data as $row) {
		$ids[] = current($row);
	}

	// 最大のidを取得
	$max_id = MAX($ids);

	// 現在時刻を取得
	date_default_timezone_set('Asia/Tokyo');
	$now_time = date("Y/m/d H:i:s");

	// 新規投稿するデータを作成
	$new_data = [$max_id + 1, $name, $msg, $now_time, $now_time];

	// 分割した既存掲示板書き込みに追加する
	$segment_data[] = $new_data;

	return $this->renderer->render($response, 'ResultView.phtml', ['data' => $segment_data]);
});
